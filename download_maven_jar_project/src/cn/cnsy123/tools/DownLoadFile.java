package cn.cnsy123.tools;

import java.io.IOException;
import java.util.Arrays;

public class DownLoadFile {
	static String localBasePath = "D:/software/work/mavenResp/jar_resp/";
	
	static String httpSsL = "http://central.maven.org/maven2/";

	static String [] fileLast = {"jar","jar.sha1","pom","pom.sha1"};//
	
	public static void main(String[] args) {
		
		String mavenAfraid = "plexus";
		
		String [] jarFilePath = {"org/codehaus/plexus/"+ mavenAfraid +"/2.0.5/"};
		
		
		coreDownLoad(jarFilePath);
		
		
		
	}

	private static void coreDownLoad(String[] jarFilePath) {
		for(String ss : jarFilePath){
			String arr []  = ss.split("/");
			
			String fileName = arr[arr.length-2] + "-" + arr[arr.length-1];
			
			System.out.println(fileName);
			System.out.println(Arrays.toString(arr));
			
			for(String base :fileLast){
				try {
					
					String nowFileName = fileName + "." + base;
					String downLoadUrl = httpSsL + ss + nowFileName;
					String savePath = localBasePath + ss;
					System.out.println("下载地址："+ downLoadUrl);
					System.out.println("下载文件："+ nowFileName);
					System.out.println("保存地址："+ savePath);
					HttpUtils.downLoadFromUrl(downLoadUrl, nowFileName, savePath);
					System.out.println("下载完成："+ nowFileName);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	

}
